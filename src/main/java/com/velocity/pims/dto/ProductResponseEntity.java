package com.velocity.pims.dto;

import lombok.Data;
import java.util.List;

@Data
public class ProductResponseEntity {
        private Long id;
        private String name;
        private double price;
        private int stockQuantity;
        private List<SupplierResponseEntity> suppliers;
        private List<String> images;
}
