package com.velocity.pims.dto;

import lombok.Data;

@Data
public class SupplierResponseEntity {

    private Long id;
    private String name;
    private String contactInfo;
}
