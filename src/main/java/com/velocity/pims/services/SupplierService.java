package com.velocity.pims.services;

import com.velocity.pims.dto.SupplierResponseEntity;
import com.velocity.pims.exceptions.ResourceNotFoundException;
import com.velocity.pims.models.SupplierEntity;
import com.velocity.pims.repositories.SupplierRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class SupplierService {

    private static final Logger logger = LoggerFactory.getLogger(SupplierService.class);

    @Autowired
    private SupplierRepository supplierRepository;

    @Autowired
    private SupplierMapper supplierMapper;

    public List<SupplierResponseEntity> getAllSuppliers() {
        logger.info("Fetching all suppliers");
        List<SupplierEntity> suppliers = supplierRepository.findAll();
        return suppliers.stream()
                .map(supplierMapper::convertToDto)
                .collect(Collectors.toList());
    }

    public SupplierResponseEntity getSupplierById(Long id) {
        logger.info("Fetching supplier with id: {}", id);
        SupplierEntity supplier = supplierRepository.findById(id)
                .orElseThrow(() -> {
                    logger.error("Supplier not found with id: {}", id);
                    return new ResourceNotFoundException("Supplier not found with id: " + id);
                });
        return supplierMapper.convertToDto(supplier);
    }

    public SupplierResponseEntity saveSupplier(SupplierEntity supplierEntity) {
        logger.info("Saving supplier: {}", supplierEntity);
        return supplierMapper.convertToDto(supplierRepository.save(supplierEntity));
    }

    public SupplierResponseEntity updateSupplier(Long id, SupplierEntity supplierEntity) {
        logger.info("Updating supplier with id: {}", id);
        SupplierEntity supplier = supplierRepository.findById(id)
                .orElseThrow(() -> {
                    logger.error("Supplier not found with id: {}", id);
                    return new ResourceNotFoundException("Supplier not found with id: " + id);
                });
        supplier.setName(supplierEntity.getName());
        supplier.setContactInfo(supplierEntity.getContactInfo());
        return supplierMapper.convertToDto(supplierRepository.save(supplier));
    }

    public void deleteSupplier(Long id) {
        logger.info("Deleting supplier with id: {}", id);
        if (!supplierRepository.existsById(id)) {
            logger.error("Supplier not found with id: {}", id);
            throw new ResourceNotFoundException("Supplier not found with id: " + id);
        }
        supplierRepository.deleteById(id);
    }
}
