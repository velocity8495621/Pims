package com.velocity.pims.services;

import com.velocity.pims.dto.ProductResponseEntity;
import com.velocity.pims.dto.SupplierResponseEntity;
import com.velocity.pims.models.ProductEntity;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class ProductMapper {

    private final ModelMapper modelMapper;

    @Autowired
    private SupplierMapper supplierMapper;

    public ProductMapper(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    public ProductResponseEntity convertToDto(ProductEntity product) {
        ProductResponseEntity productDto = modelMapper.map(product, ProductResponseEntity.class);

        List<SupplierResponseEntity> supplierDtos = product.getSuppliers().stream()
                .map(supplierMapper::convertToDto)
                .collect(Collectors.toList());

        productDto.setSuppliers(supplierDtos);
        return productDto;
    }
}
