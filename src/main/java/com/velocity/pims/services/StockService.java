package com.velocity.pims.services;

import com.velocity.pims.models.ProductEntity;
import com.velocity.pims.models.StockEntity;
import com.velocity.pims.repositories.ProductRepository;
import com.velocity.pims.repositories.StockRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StockService {

    @Autowired
    private StockRepository stockRepository;



    public StockEntity getStockByProductId(Long productId) {
        // Assuming that each product has a unique stock
        return stockRepository.findByProductId(productId);
    }

    public void updateStockByProductId(Long productId, int newQuantity) {
        StockEntity stock = stockRepository.findByProductId(productId);
        if (stock != null) {
            stock.setQuantity(newQuantity);
            stockRepository.save(stock);
        } else {
            throw new RuntimeException("Stock not found for product ID: " + productId);
        }
    }
}
