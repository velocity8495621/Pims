package com.velocity.pims.services;

import com.velocity.pims.dto.ProductResponseEntity;
import com.velocity.pims.exceptions.ResourceNotFoundException;
import com.velocity.pims.models.ProductEntity;
import com.velocity.pims.models.SupplierEntity;
import com.velocity.pims.repositories.ProductRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ProductService {

    private static final Logger logger = LoggerFactory.getLogger(ProductService.class);

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private SupplierService supplierService;

    @Autowired
    private ProductMapper productMapper;

    public List<ProductResponseEntity> getAllProducts(Long supplierId, Double minPrice, Double maxPrice) {
        logger.info("Fetching all products");
        List<ProductEntity> products;
        if (supplierId != null && minPrice != null && maxPrice != null) {
            logger.info("Fetching products by supplierId: {}, minPrice: {}, maxPrice: {}", supplierId, minPrice, maxPrice);
            products = productRepository.findBySupplierIdAndPriceRange(supplierId, minPrice, maxPrice);
        } else if (supplierId != null) {
            logger.info("Fetching products by supplierId: {}", supplierId);
            products = productRepository.findBySupplierId(supplierId);
        } else {
            logger.info("Fetching all products");
            products = productRepository.findAll();
        }

        return products.stream()
                .map(productMapper::convertToDto)
                .collect(Collectors.toList());
    }

    public ProductResponseEntity getProductById(Long id) {
        logger.info("Fetching product by id: {}", id);
        ProductEntity product = productRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Product not found with id: " + id));
        return productMapper.convertToDto(product);
    }

    public ProductResponseEntity saveProduct(ProductEntity product) {
        logger.info("Saving product: {}", product);
        List<SupplierEntity> supplierEntities = product.getSuppliers();
        if (supplierEntities != null && !supplierEntities.isEmpty()) {
            for (SupplierEntity supplierEntity : supplierEntities) {
                supplierService.saveSupplier(supplierEntity);
            }
        }
        ProductEntity savedProduct = productRepository.save(product);
        return productMapper.convertToDto(savedProduct);
    }

    public void deleteProduct(Long id) {
        logger.info("Deleting product with id: {}", id);
        if (!productRepository.existsById(id)) {
            throw new ResourceNotFoundException("Product not found with id: " + id);
        }
        productRepository.deleteById(id);
    }

    public ProductResponseEntity updateProduct(Long id, ProductEntity productDetails) {
        try {
            logger.info("Updating product with id: {}", id);
            ProductEntity product = productRepository.findById(id)
                    .orElseThrow(() -> new ResourceNotFoundException("Product not found with id: " + id));

            // Update product details
            product.setName(productDetails.getName());
            product.setPrice(productDetails.getPrice());
            product.setStockQuantity(productDetails.getStockQuantity());
            product.setSuppliers(productDetails.getSuppliers());
            product.setImages(productDetails.getImages());
            List<SupplierEntity> supplierEntities = product.getSuppliers();
            if (supplierEntities != null && !supplierEntities.isEmpty()) {
                for (SupplierEntity supplierEntity : supplierEntities) {
                    supplierService.saveSupplier(supplierEntity);
                }
            }
            ProductEntity updatedProduct = productRepository.save(product);
            return productMapper.convertToDto(updatedProduct);
        } catch (Exception e) {
            logger.error("Error updating product with id: " + id, e);
            throw e; // Rethrow the exception to be handled by the controller
        }
    }
}
