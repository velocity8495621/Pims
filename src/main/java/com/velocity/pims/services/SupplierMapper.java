package com.velocity.pims.services;

import com.velocity.pims.dto.SupplierResponseEntity;
import com.velocity.pims.models.SupplierEntity;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class SupplierMapper {

    @Autowired
    private ModelMapper modelMapper;

    public SupplierResponseEntity convertToDto(SupplierEntity supplier) {
        return modelMapper.map(supplier, SupplierResponseEntity.class);
    }

}
