package com.velocity.pims.models;


import jakarta.persistence.*;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Entity
@Table(name = "product")  // Specifying the table name
@Data
public class ProductEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;
    private double price;

    @OneToOne(mappedBy = "product", cascade = CascadeType.ALL)
    private StockEntity stockQuantity;

    @OneToMany
    private List<SupplierEntity> suppliers;

    @ElementCollection
    private List<String> images;

    // Getters and setters
}
