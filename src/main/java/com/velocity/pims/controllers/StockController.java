package com.velocity.pims.controllers;

import com.velocity.pims.models.StockEntity;
import com.velocity.pims.services.StockService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/stocks")
public class StockController {

    @Autowired
    private StockService stockService;

    @GetMapping("/{productId}")
    public ResponseEntity<StockEntity> getStockByProductId(@PathVariable Long productId) {
        StockEntity stock = stockService.getStockByProductId(productId);
        if (stock == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(stock);
    }

    @PutMapping("/{productId}/update")
    public ResponseEntity<Void> updateStockByProductId(@PathVariable Long productId, @RequestParam int newQuantity) {
        stockService.updateStockByProductId(productId, newQuantity);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }
}
