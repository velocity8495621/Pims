package com.velocity.pims.controllers;

import ch.qos.logback.classic.Logger;
import com.velocity.pims.dto.ProductResponseEntity;
import com.velocity.pims.exceptions.ResourceNotFoundException;
import com.velocity.pims.models.ProductEntity;
import com.velocity.pims.services.ProductService;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/products")
public class ProductController {

    @Autowired
    private ProductService productService;

    private static final Logger logger = (Logger) LoggerFactory.getLogger(ProductController.class);


    @GetMapping
    public List<ProductResponseEntity> getAllProducts(
            @RequestParam(required = false) Long supplierId,
            @RequestParam(required = false) Double minPrice,
            @RequestParam(required = false) Double maxPrice) {
        logger.info("Fetching all products...");
        return productService.getAllProducts(supplierId, minPrice, maxPrice);
    }

    @GetMapping("/{id}")
    public ResponseEntity<ProductResponseEntity> getProductById(@PathVariable Long id) {
        try {
            logger.info("Fetching product with id: {}", id);
            ProductResponseEntity product = productService.getProductById(id);
            return ResponseEntity.ok(product);
        } catch (ResourceNotFoundException e) {
            logger.error("Product not found with id: {}", id);
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping
    public ResponseEntity<ProductResponseEntity> createProduct(@RequestBody ProductEntity product) {
        logger.info("Creating new product...");
        ProductResponseEntity savedProduct = productService.saveProduct(product);
        return new ResponseEntity<>(savedProduct, HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<ProductResponseEntity> updateProduct(@PathVariable Long id, @RequestBody ProductEntity productDetails) {
        try {
            logger.info("Updating product with id: {}", id);
            ProductResponseEntity updatedProduct = productService.updateProduct(id, productDetails);
            return ResponseEntity.ok(updatedProduct);
        } catch (ResourceNotFoundException e) {
            logger.error("Product not found with id: {}", id);
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteProduct(@PathVariable Long id) {
        try {
            logger.info("Deleting product with id: {}", id);
            productService.deleteProduct(id);
            return ResponseEntity.noContent().build();
        } catch (ResourceNotFoundException e) {
            logger.error("Product not found with id: {}", id);
            return ResponseEntity.notFound().build();
        }
    }
}
