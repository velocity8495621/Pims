package com.velocity.pims.controllers;

import com.velocity.pims.dto.SupplierResponseEntity;
import com.velocity.pims.exceptions.ResourceNotFoundException;
import com.velocity.pims.models.SupplierEntity;
import com.velocity.pims.services.SupplierService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/suppliers")
public class SupplierController {

    private static final Logger logger = LoggerFactory.getLogger(SupplierController.class);

    @Autowired
    private SupplierService supplierService;

    @GetMapping
    public List<SupplierResponseEntity> getAllSuppliers() {
        logger.info("Fetching all suppliers");
        return supplierService.getAllSuppliers();
    }

    @GetMapping("/{id}")
    public ResponseEntity<SupplierResponseEntity> getSupplierById(@PathVariable Long id) {
        logger.info("Fetching supplier with id {}", id);
        try {
            SupplierResponseEntity supplier = supplierService.getSupplierById(id);
            return ResponseEntity.ok(supplier);
        } catch (ResourceNotFoundException e) {
            logger.error("Supplier with id {} not found", id);
            return ResponseEntity.notFound().build();
        } catch (Exception e) {
            logger.error("An error occurred while fetching supplier with id {}", id, e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @PostMapping
    public ResponseEntity<SupplierResponseEntity> createSupplier(@RequestBody SupplierEntity supplier) {
        logger.info("Creating new supplier");
        try {
            SupplierResponseEntity createdSupplier = supplierService.saveSupplier(supplier);
            return ResponseEntity.ok(createdSupplier);
        } catch (Exception e) {
            logger.error("An error occurred while creating a new supplier", e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<SupplierResponseEntity> updateSupplier(@PathVariable Long id, @RequestBody SupplierEntity supplierDetails) {
        logger.info("Updating supplier with id {}", id);
        try {
            SupplierResponseEntity updatedSupplier = supplierService.updateSupplier(id, supplierDetails);
            return ResponseEntity.ok(updatedSupplier);
        } catch (ResourceNotFoundException e) {
            logger.error("Supplier with id {} not found", id);
            return ResponseEntity.notFound().build();
        } catch (Exception e) {
            logger.error("An error occurred while updating supplier with id {}", id, e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteSupplier(@PathVariable Long id) {
        logger.info("Deleting supplier with id {}", id);
        try {
            supplierService.deleteSupplier(id);
            return ResponseEntity.noContent().build();
        } catch (ResourceNotFoundException e) {
            logger.error("Supplier with id {} not found", id);
            return ResponseEntity.notFound().build();
        } catch (Exception e) {
            logger.error("An error occurred while deleting supplier with id {}", id, e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
}
