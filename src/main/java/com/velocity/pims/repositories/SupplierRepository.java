package com.velocity.pims.repositories;

import com.velocity.pims.models.SupplierEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SupplierRepository extends JpaRepository<SupplierEntity,Long> {
}
