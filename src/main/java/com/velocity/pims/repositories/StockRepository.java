package com.velocity.pims.repositories;

import com.velocity.pims.models.StockEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface StockRepository extends JpaRepository<StockEntity, Long> {

    @Query(value="SELECT * FROM stock s WHERE s.product_id = ?1",nativeQuery = true)
    StockEntity findByProductId(Long productId);
}
