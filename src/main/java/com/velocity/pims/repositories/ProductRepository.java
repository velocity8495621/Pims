package com.velocity.pims.repositories;


import com.velocity.pims.models.ProductEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductRepository extends JpaRepository<ProductEntity, Long> {
    // Method to find products by supplier ID
    @Query(value = "SELECT * FROM Product p INNER JOIN Product_suppliers p1 ON p1.product_entity_id = p.id WHERE p1.suppliers_id =:supplierId", nativeQuery = true)
    List<ProductEntity> findBySupplierId(Long supplierId);

    // Custom query to find products by supplier ID and price range
    @Query(value = "SELECT p FROM Product p " +
            "WHERE p.supplierId = :supplierId AND p.price " +
            "BETWEEN :minPrice AND :maxPrice", nativeQuery = true)
    List<ProductEntity> findBySupplierIdAndPriceRange(@Param("supplierId") Long supplierId,
                                                      @Param("minPrice") Double minPrice,
                                                      @Param("maxPrice") Double maxPrice);
}

