package com.velocity.pims;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PIMSApplication {

    public static void main(String[] args) {
        SpringApplication.run(PIMSApplication.class, args);
    }

}