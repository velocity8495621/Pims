# Application Deployment using Docker

This guide will walk you through the process of deploying our application using Docker containers. Docker provides a convenient way to package and distribute applications along with their dependencies, ensuring consistency across different environments.

## Prerequisites

Before you begin, ensure that you have the following installed on your system:

- **Java 17:** Make sure you have Java version 17 installed on your system. You can download and install Java from the [official Java website](https://www.oracle.com/java/technologies/javase-jdk17-downloads.html).
- **Docker:** Docker needs to be installed on your system. You can download and install Docker from the [official Docker website](https://www.docker.com/get-started).
- **NOTE:** Make Sure docker is running in your system.
- **Tech Stack:** Java 17,SpringBoot,H2 in-memory DB,maven,Hibernate.

## Getting Started

1. Clone the repository:

    ```bash
    git clone https://gitlab.com/velocity8495621/Pims.git
    ```

2. Navigate to the project directory:

    ```bash
    cd Pims-master
    ```

3. Build the Docker image:

    ```bash
    docker build -t <image_name> .
    ```

   Replace `<image_name>` with the desired name for your Docker image.

4. Run the Docker container:

    ```bash
    docker run -d -p <host_port>:<container_port> <container_name>:<tag>
    ```
    The port exposed is 8080
   Replace `<host_port>` and `<container_port>` with the ports your application listens on. `<container_name>` is the name you want to assign to your container.

5. Access the application

   Once the container is running, you can access the application by navigating to `http://localhost:<host_port>` in any of the testing agent i.e Postman.

## Configuration

- **Environment Variables:** You can customize the application configuration using environment variables. Refer to the `Dockerfile` or `docker-compose.yml` file for a list of supported environment variables.

## Maintenance

- **Stopping the Container:** To stop the running container, use the following command:

    ```bash
    docker stop <container_name>
    ```

- **Removing the Container:** To remove the container, use the following command:

    ```bash
    docker rm <container_name>
    ```

- **Removing the Image:** To remove the Docker image, use the following command:

    ```bash
    docker rmi <image_name>
    ```

## Troubleshooting

- **Logs:** You can view the application logs using the following command:

    ```bash
    docker logs <container_name>
    ```

## Example Docker Commands

- List Docker images:
    ```bash
    docker images
    ```

- List running Docker containers:
    ```bash
    docker ps
    ```

- List all Docker containers (including stopped ones):
    ```bash
    docker ps -a
    ```

- Build a Docker image with the tag `pims:1.0`:
    ```bash
    docker build -t pims:1.0 .
    ```

- Run a Docker container with the image `pims:1.0` in detached mode, mapping port 8080:
    ```bash
    docker run -d -p 8080:8080 pims:1.0
    ```

- Stop all running containers:
    ```bash
    docker stop $(docker ps -aq)
    ```

- Remove all containers (both running and stopped):
    ```bash
    docker rm $(docker ps -aq)
    ```

- Remove all Docker images:
    ```bash
    docker rmi $(docker images -q)
    ```

- Stop all running containers, remove all containers, and remove all images (in one command):
    ```bash
    docker stop $(docker ps -aq) && docker rm $(docker ps -aq) && docker rmi $(docker images -q)
    ```

## Without Docker

Incase of troubles using Docker please follow the below steps to test the application without docker
- Build the Project
- ```bash
     mvn clean package   
    ```    
- navigate to target directory and run the following command
  ```bash
    java -jar Product_Inventory_Management_System-0.0.1-SNAPSHOT.jar
   ```
- Now the springBoot application must have been started and can access using postman agent

## Documentation
For all the api documentatio and schema please refer the url once the server is started
http://localhost:8080/swagger-ui/index.html#/

## Contributing

Contributions are welcome! If you encounter any issues or have suggestions for improvements, please feel free to open an issue or submit a pull request.


